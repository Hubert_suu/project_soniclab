import React from "react";
import { Link, withRouter } from "react-router-dom";

const Navigation =(props)=>{
    return (
        <div className="navigation  fixed-top">
            <div class="container-fluid">
                <div class="row">
                    <div class=" con1 col-xl-12 col-md-12 col-sm-12">
                              <div class="row bar">
                                  
    
                                              <ul class=" col-12 navbar-nav"> 
                                                    <li class={` ${
                                                            props.location.pathname === "/Home" ? "active" : ""
                                                            }`}>
                                                             <Link class="nav-link" to="/PineApple"> Home</Link>
                                                        
                                                    </li>     
                                                   
                                            </ul>
                                </div>
                                
                                
                          
                        
                    </div>
                </div>
               
               
            </div>
        </div>
      );
}
export default withRouter (Navigation);