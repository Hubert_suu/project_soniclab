import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import $ from 'jquery';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import Product from './Product';

const Index = () =>{
    return(
        <div>
            <Router>
                <Navbar />
            
            <Switch>
                <Route path="/" exact><Home /></Route>
                <Route path="/about"><About /></Route>
                <Route path="/product"><Product /></Route>
                <Route path="/contact"><Contact/></Route>
            </Switch>
            <Footer />
            </Router>
        </div>
    )
}
export default Index;